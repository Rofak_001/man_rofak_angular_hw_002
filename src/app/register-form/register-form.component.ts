import {Component, OnInit} from '@angular/core';
import {Form, FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {MustMatch} from '../Validators/mustMatchPassword';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit {
  formRegister;
  submitted = false;
  modal = 'modal';
  Data: Array<any> = [
    {name: 'A Friend or college', value: 'A Friend or college'},
    {name: 'Google', value: 'Google'},
    {name: 'Blog', value: 'Blog'},
    {name: 'News Article', value: 'News Article'},
  ];
  days = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31];
  months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
  years = [2019, 2018, 2017, 2016, 2015, 2014, 2013, 2012, 2011, 2010, 2009, 2008, 2007, 2006, 2005, 2004, 2003, 2002, 2001, 2000];

  constructor() {
    this.formRegister = new FormGroup({
      firstName: new FormControl('', [Validators.required, Validators.maxLength(32)]),
      lastName: new FormControl('', [Validators.required, Validators.maxLength(32)]),
      email: new FormControl('', [Validators.required, Validators.email]),
      AreaCode: new FormControl('', [Validators.required, Validators.pattern('^[0-9]*$')]),
      PhoneNumber: new FormControl('', [Validators.required, Validators.pattern('^[0-9]*$')]),
      password: new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(128)]),
      confirmPass: new FormControl('', [Validators.required]),
      address: new FormControl('', [Validators.required]),
      addressline2: new FormControl(''),
      city: new FormControl('', [Validators.required]),
      state: new FormControl('', [Validators.required]),
      zipCode: new FormControl('', [Validators.required, Validators.pattern('^[0-9]*$')]),
      country: new FormControl('', [Validators.required]),
      day: new FormControl('', [Validators.required]),
      month: new FormControl('', [Validators.required]),
      year: new FormControl('', [Validators.required]),
      groupCheckBox: new FormArray([], [Validators.required]),
      memberShip: new FormControl(false, [Validators.requiredTrue]),
      policy: new FormControl(false, [Validators.requiredTrue])
    }, {
      validators: MustMatch('password', 'confirmPass')
    });
  }

  get firstName(): FormControl {
    return this.formRegister.get('firstName');
  }

  get lastName(): FormControl {
    return this.formRegister.get('lastName');
  }

  get email(): FormControl {
    return this.formRegister.get('email');
  }

  get areaCode(): FormControl {
    return this.formRegister.get('AreaCode');
  }

  get phoneNumber(): FormControl {
    return this.formRegister.get('PhoneNumber');
  }

  get password(): FormControl {
    return this.formRegister.get('password');
  }

  get confirmPassword(): FormControl {
    return this.formRegister.get('confirmPass');
  }

  get address(): FormControl {
    return this.formRegister.get('address');
  }

  get city(): FormControl {
    return this.formRegister.get('city');
  }

  get state(): FormControl {
    return this.formRegister.get('state');
  }

  get zipCode(): FormControl {
    return this.formRegister.get('zipCode');
  }

  get country(): FormControl {
    return this.formRegister.get('country');
  }

  get groupCheckbox(): FormArray {
    return this.formRegister.get('groupCheckBox') as FormArray;
  }

  get memberShip(): FormControl {
    return this.formRegister.get('memberShip');
  }

  get policy(): FormControl {
    return this.formRegister.get('policy');
  }

  get day(): FormControl {
    return this.formRegister.get('day');
  }

  get month(): FormControl {
    return this.formRegister.get('month');
  }

  get year(): FormControl {
    return this.formRegister.get('year');
  }

  onSubmit(): void {
    this.submitted = true;
    console.log(this.formRegister);
    alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.formRegister.value, null, 4));
  }

  ngOnInit(): void {
  }

  onCheckboxChange(e): void {
    const checkArray: FormArray = this.formRegister.get('groupCheckBox') as FormArray;

    if (e.target.checked) {
      checkArray.push(new FormControl(e.target.value));
    } else {
      let i = 0;
      checkArray.controls.forEach((item: FormControl) => {
        if (item.value === e.target.value) {
          checkArray.removeAt(i);
          return;
        }
        i++;
      });
    }
  }
}
